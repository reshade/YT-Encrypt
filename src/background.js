/* global chrome */
const MessageType = {
    PAGE_UPDATED: 'pageUpdated'
  };

//Checks if page is loaded 
chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, _tab){
    if(changeInfo.hasOwnProperty("status") && changeInfo.status == "complete") {
        chrome.tabs.sendMessage(tabId, { type: MessageType.PAGE_UPDATED });
    }
});