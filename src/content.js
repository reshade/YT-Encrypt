/* global chrome */
const MessageType = {
    PAGE_UPDATED: 'pageUpdated'
};

let observer;

//Check if updated tab is video page;
chrome.runtime.onMessage.addListener(function(request, _sender, _sendResponse) {
    if (request && request.type === MessageType.PAGE_UPDATED && /\/watch\?v=/.test(window.location.href)) {
        //Observe
        observe();
    }
});

//Observes DOM Changes until user opens the comment box, and/or observes all comments
async function observe() {
    /*let settings = await new Promise(function(resolve, reject){
        //chrome.storage.sync.get(["comment","include","link"], function(settings){
        chrome.storage.sync.get(["comment","include"], function(settings){
            resolve(settings);
        })
    });

    let scanComments = false // settings.comment || settings.link
    let r = /\[(.*?[^\\])\]/gm
    let texts =  r.exec(settings.include.toLowerCase())*/

    let foundCommentbox = false;
    let foundCreator = false;
    let sparkles = false;
    let commentdialog = null;

    //New Observer
    observer?.disconnect();
    observer = new MutationObserver(function(mutations) {
        mutations.forEach(function(mutation) {            
        if (mutation.addedNodes.length) {

            //If its not Sparkles stop observing
            if(!foundCreator){
                const child = document.querySelector("ytd-video-owner-renderer")?.querySelector(".yt-simple-endpoint.style-scope.yt-formatted-string");
                if(child) {
                    if(!child.href.includes("UCs3GloeEzu5rDlQlSLGrr4A")) {
                        observer.disconnect();
                        remove();
                    }
                    foundCreator = true;
                    sparkles = true;
                }
            }

            //Check if the commentbox is found
            if(!foundCommentbox){

                commentdialog = commentdialog || document.getElementById('comment-dialog')

                if(commentdialog?.querySelector('#footer')) {
                    foundCommentbox = true;
                    //Remove any previous buttons
                    remove()
                    //Insert if Sparkles Video
                    if(sparkles) {
                        insert();
                        sparkles = false
                        if(!scanComments){
                            observer.disconnect();
                        }
                    }
                }
            }
        }

        //Check for Comment
        if(scanComments){
            mutation.addedNodes.forEach(function(node) {
                if(node.nodeName === "YTD-COMMENT-THREAD-RENDERER"){
                    for(let i = 0; i < texts.length; i++) {
                        if(node.textContent.toLowerCase().includes(texts[i])) {
                            node.parentElement.removeChild(node)
                            break
                        }
                    }
                }
            });
        }

        });
    });

    //Observe Document Changes
    observer.observe(document.body, {
        childList: true,
        subtree: true,
        attributes: false,
        characterData: false
    });
}

function remove() {
    while(true) { //Loop in case of issues
        const button = document.getElementById("encrypt-button");
        if(button == null) {
            break
        }
        button.parentElement.removeChild(button);
    }
}

//Inserts an Encrypt Button under the comment field
function insert() {
    const button = document.createElement('button');
    button.setAttribute("id", "encrypt-button");
    button.style.cssText = 'background-color: #ff4f00; color: var(--yt-spec-general-background-a); padding: 10px 16px; border: none; border-radius: 2px; text-align: center; font-family: Roboto; font-weight: 500; font-size: 14px; font-style: normal; letter-spacing: 0.007px;';
    button.innerHTML = "Encrypt";
    button.addEventListener("click", findandreplace);

    const footer = document.getElementById('comment-dialog').querySelector('#footer');
    footer.insertBefore(button, footer.firstElementChild);
}

//Encrypts any found tradelink
async function findandreplace() {

    //Find Commentbox
    const content = document.getElementById('comment-dialog').querySelector('#contenteditable-root');
    let cipher;
    for (let node of content.childNodes) {
        //Find Tradelink
        let tradelink = /\d+&token=\S+/.exec(node.textContent);
        if (tradelink === null) {
            continue;
        }

        //Encrypt and Replace
        cipher = await encrypt(tradelink[0]);
        node.textContent = node.textContent.replace(/\S*\d+&token=\S+/, "#TRADE" + cipher);
        return;
    }
}

//Encrypt String with RSA encryption with SubtleCrypto
async function encrypt(str) {
    //Get Pub Key from Description
    let desc = document.querySelector('div[id=description]');
    let pub = /MIG\S+QAB/.exec(desc.textContent);
    if(pub === null) {
        return " Error: No Key Found";
    }
    pub = pub[0].replace(/\r\n|\n|\r/g,"");

    const binaryDer = str2ab(window.atob(pub));

    const cryptokey = await window.crypto.subtle.importKey("spki",
        binaryDer,
        {
            name: "RSA-OAEP",
            hash: "SHA-256"
        },
        true, ["encrypt"]);

    //Encrypt
    const encoder = new TextEncoder();
    const encoded = encoder.encode(str);

    const buf = await window.crypto.subtle.encrypt({
            name: "RSA-OAEP"
        }, cryptokey,
        encoded);
    return ab2B64(buf);
}

//Convert String to ArrayBuffer
function str2ab(str) {
    const buf = new ArrayBuffer(str.length);
    const bufView = new Uint8Array(buf);
    for (let i = 0, strLen = str.length; i < strLen; i++) {
      bufView[i] = str.charCodeAt(i);
    }
    return buf;
}

//Convert ArrayBuffer to Base64
function ab2B64(buf) {
    let str = '';
    const bytes = new Uint8Array(buf);
    const len = bytes.byteLength;
    for (let i = 0; i < len; i++) {
        str += String.fromCharCode(bytes[i]);
    }
    return window.btoa(str);
}
