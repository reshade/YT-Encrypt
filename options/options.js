function saveOptions(e) {
  e.preventDefault();
  chrome.storage.sync.set({
    comment: document.querySelector('#comment').checked,
    include: document.querySelector('#include').value,
    link: document.querySelector("#link").checked
  });
}

function restoreOptions() {

  function setCurrentChoice(result) {
    document.querySelector("#comment").checked = result.comment || false;
    document.querySelector("#include").value = result.include || "\"I'll send a trade Link if i win\",\"I'll send trade link if i win\",";
    document.querySelector("#link").checked = result.link || false;
  }

  chrome.storage.sync.get(["comment","include","link"], setCurrentChoice);
}

document.addEventListener("DOMContentLoaded", restoreOptions);
document.querySelector("form").addEventListener("submit", saveOptions);
